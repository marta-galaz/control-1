package cl.duoc.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnEntar,btnRegistro;
    private EditText etUsuario,etClave;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsuario =(EditText)findViewById(R.id.etUsuario);
        etClave=(EditText)findViewById(R.id.etPass);
        btnEntar=(Button)findViewById(R.id.btnIngresar);
        btnRegistro=(Button)findViewById(R.id.btnRegistro);

        btnEntar.setOnClickListener(this);
        btnRegistro.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btnIngresar){
            if (etClave.getText().toString().equals("admin") && etUsuario.getText().toString().equals("admin")){
                Toast.makeText(LoginActivity.this, "Usuario Valido", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(LoginActivity.this, "Usuario no Valido", Toast.LENGTH_LONG).show();
            }
        }else if (v.getId() == R.id.btnRegistro){
            Intent i = new Intent(LoginActivity.this,RegistroActivity.class);
            startActivity(i);

        }

    }

}
