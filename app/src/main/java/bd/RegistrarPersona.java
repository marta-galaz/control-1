package bd;

import java.util.ArrayList;
import Entidades.Persona;

/**
 * Created by DUOC on 24-03-2017.
 */

public class RegistrarPersona {

private static ArrayList<Persona>  formularios  = new ArrayList<>();

    public static void agregarFormulario(Persona persona)
    {
        formularios.add(persona);
    }
    
    public  static ArrayList<Persona> getFormularios()
    {
        return formularios;
    }
}
