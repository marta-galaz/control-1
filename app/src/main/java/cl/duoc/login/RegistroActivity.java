package cl.duoc.login;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

import Entidades.Persona;
import bd.RegistrarPersona;

public class RegistroActivity extends AppCompatActivity {

    private Button btnRegistro, etFecha;
    private EditText etUsuario, etNombres, etApellidos, etRut, etClave1, etclave2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        etUsuario = (EditText) findViewById(R.id.etUsuario);
        etNombres = (EditText) findViewById(R.id.etNombres);
        etApellidos = (EditText) findViewById(R.id.etApellidos);
        etRut = (EditText) findViewById(R.id.etRut);
        etFecha = (Button) findViewById(R.id.etFecha);
        etClave1 = (EditText) findViewById(R.id.etClave);
        etclave2 = (EditText) findViewById(R.id.etRepetir);

        btnRegistro = (Button) findViewById(R.id.btnRegistro);


        etFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().show();
            }
        });

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrarPersona();
            }
        });

    }

    public void registrarPersona() {

        String mensajeError="";
        if(etUsuario.getText().toString().length()<1)
        {
            mensajeError += "Ingrese algún Usuario \n";
        }
        if(etNombres.getText().toString().length()<1)
        {
            mensajeError += "Ingrese Nombres \n";
        }
        if(etApellidos.getText().toString().length()<1)
        {
            mensajeError += "Ingrese Apellidos \n";
        }
        if(!isValidarRut(etRut.getText().toString()))
        {
            mensajeError += "Ingrese un RUT válido \n";
        }
        if(etClave1.getText().toString().length()<1)
        {
            mensajeError += "Ingrese una  Clave \n";
        }
        if(etclave2.getText().toString().length()<1)
        {
            mensajeError += "Ingrese repetición de clave \n";
        }
        if(!etClave1.getText().toString().equals(etclave2.getText().toString())) {
            mensajeError += "Las claves no son iguales \n";
        }
        if (mensajeError.length()>0)
        {
            Toast.makeText(this, mensajeError, Toast.LENGTH_LONG).show();
        }else
        {

            Persona persona = new Persona();
            persona.setUsuario(etUsuario.getText().toString());
            persona.setNombres(etNombres.getText().toString());
            persona.setApellidos(etApellidos.getText().toString());
            persona.setRut(etRut.getText().toString());
            persona.setFecha(etFecha.getText().toString());
            persona.setClave(etClave1.getText().toString());
            persona.setRepetirClave(etclave2.getText().toString());

            RegistrarPersona.agregarFormulario(persona);
            Toast.makeText(this,"Formulario guardado correctamente",Toast.LENGTH_LONG).show();
        }

    }


    private DatePickerDialog getDialog() {
        Calendar c = Calendar.getInstance();
        int anio = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH);
        int dia = c.get(Calendar.DAY_OF_MONTH);


        return new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                etFecha.setText(year + "/" + formatDate((month + 1)) + "/" + formatDate(dayOfMonth));
            }
        }, anio, mes, dia);

    }

    private String formatDate(int value) {

        return (value > 9 ? value + "" : "0" + value);

    }


    private boolean isValidarRut(String rut) {

        boolean validacion = false;
        try {
            rut =  rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }
}

